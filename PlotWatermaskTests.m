%% load watermasks.mat

clear
load Output/AccWatermasks.mat

firstVideo = 1;
lastVideo =  74;
nVideos = lastVideo-firstVideo+1;

%extract values from the structures
%get dates
dates = zeros(nVideos ,1);
for i=firstVideo:lastVideo
    dates(i-firstVideo+1) = datenum(result(i).Date);
end
indNaN = find(isnan(dates));
indNOTNaN = find(~isnan(dates));
dates = dates(indNOTNaN);

% get watermasks accuracies
waterMaskAccFull = zeros(length(indNOTNaN),3);
waterMaskAccFirst = zeros(length(indNOTNaN),3);
waterMaskAccSecond = zeros(length(indNOTNaN),3);
waterMaskAccThird = zeros(length(indNOTNaN),3);
for i=1:length(indNOTNaN)
    i = indNOTNaN(i);
    waterMaskAccFull(i-firstVideo+1, 1:3)= result(i).AccWaterMask;
    waterMaskAccFirst(i-firstVideo+1, 1:3) = result(i).FirstProfil(1).AccWaterMask;
    waterMaskAccSecond(i-firstVideo+1, 1:3) = result(i).SecondProfil(1).AccWaterMask;
    waterMaskAccThird(i-firstVideo+1, 1:3) = result(i).ThirdProfil(1).AccWaterMask;
end

ind_wm = find(~isnan(waterMaskAccFull(:,1)));

[r,c,v]=find(isnan(waterMaskAccFull));
result(indNaN) = []; % remove empty entries


plot(waterMaskAccFull(:,3) ,'.', 'MarkerSize', 4, 'Marker','o',...
    'MarkerFaceColor','red', 'Color', 'red')
xlabel('Video #')
ylabel('Accuracy')
ylim([0 1])
for i = 1 : lastVideo
    line([i i], [waterMaskAccFull(i,3) waterMaskAccFull(i,1)], 'Color',  [0.2 0.2 0.8]);
end

for i = 1 : lastVideo
    line([i i], [waterMaskAccFull(i,3) waterMaskAccFull(i,2)], 'Color', [0.1 0.8 0.1]);
end
hold on;
plot(waterMaskAccFull(:,3) ,'.', 'MarkerSize', 4, 'Marker','o',...
    'MarkerFaceColor','red', 'Color', 'red')

saveas(gcf,'Output/WatermaskAccuracies', 'pdf')

hold off;

hist(waterMaskAccFull(:,3),20)
xlabel('F1-score')
ylabel('Count')
line([nanmedian(waterMaskAccFull(:,3)) nanmedian(waterMaskAccFull(:,3))], ...
    [0 14])
text(0.83,13, num2str(nanmedian(waterMaskAccFull(:,3)),2), 'FontSize',14)

saveas(gcf,'Output/WatermaskAccuraciesHist', 'pdf')


