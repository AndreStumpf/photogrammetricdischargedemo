%% Compute discharge from videos at the dates of reference measurements

%% Load

% load camera calibrations
load Cameras/Cam1/Cam_CalTsai Cam_CalTsai;
Cam1_CalTsai = Cam_CalTsai;
load Cameras/Cam2/Cam_CalTsai Cam_CalTsai;
Cam2_CalTsai = Cam_CalTsai;
 
% load all DSMs into one structure
DSMstruct=struct('Date',{},'Camera',{},'DSM',{});
DSMstruct(1).Date = [2013 11 05 12 0 0];
DSMstruct(1).Camera = 'Cam1';
DSMstruct(1).DSM = load('Cameras/Cam1/DSM_Cam1_2013-11-05');
DSMstruct(2).Date = [2013 11 05 12 0 0];
DSMstruct(2).Camera = 'Cam2';
DSMstruct(2).DSM = load('Cameras/Cam2/DSM_Cam2_2013-11-05');
DSMstruct(3).Date = [2014 05 13 12 0 0];
DSMstruct(3).Camera = 'Cam1';
DSMstruct(3).DSM = load('Cameras/Cam1/DSM_Cam1_2014-05-13');
DSMstruct(4).Date = [2014 05 13 12 0 0];
DSMstruct(4).Camera = 'Cam2';
DSMstruct(4).DSM = load('Cameras/Cam2/DSM_Cam2_2014-05-13');
DSMstruct(5).Date = [2014 12 02 12 0 0];
DSMstruct(5).Camera = 'Cam1';
DSMstruct(5).DSM = load('Cameras/Cam1/DSM_Cam1_2014-12-02');
DSMstruct(6).Date = [2014 12 02 12 0 0];
DSMstruct(6).Camera = 'Cam2';
DSMstruct(6).DSM = load('Cameras/Cam2/DSM_Cam2_2014-12-02');

%% Select folder with input videos (go to videos_jaugeage)
[pathVideos, fullpathVideos] = listVideosAutoRec();

% set parameters
crop = [0, 29, 0, 0];
n_im = 30;
init_im = 1;
doPlot=1;
DepthAver=1.0;
highestFrac=0.05;
nFrames = 40;
minFrameRate = 0.5;
proj=1;
    
%% loop over all videos

% create structures to accomodate results
SurfVelocity=struct('V',{},'Vp',{});
FirstProfil=struct('Profil',{},...
    'WaterHeight',{},'HeightSkew',{},'nEmptybins',{},'MedianDischarge',{},...
    'TwoSigmaDischarge',{},'DischargePerRow',{},'AccWaterMask',{});
SecondProfil=struct('Profil',{},...
    'WaterHeight',{},'HeightSkew',{},'nEmptybins',{},'MedianDischarge',{},...
    'TwoSigmaDischarge',{},'DischargePerRow',{},'AccWaterMask',{});
ThirdProfil=struct('Profil',{},...
    'WaterHeight',{},'HeightSkew',{},'nEmptybins',{},'MedianDischarge',{},...
    'TwoSigmaDischarge',{},'DischargePerRow',{},'AccWaterMask',{});
result=struct('Date',{},'Camera',{},'Video',{},'WaterMask',{},'FirstProfil',FirstProfil,...
    'SecondProfil',SecondProfil,'ThirdProfil',ThirdProfil,'AccWaterMask',{},'SurfVelocity',SurfVelocity);


for i=1:length(fullpathVideos)
    
    fullpathVideo = fullpathVideos{i};
    disp(' ')
    disp(['Processing video ', num2str(i), ' out of ', num2str(length(fullpathVideos))])
    disp(fullpathVideo)
    [firstFrame,framdur,duration, datevideo] = getFrameInfos(fullpathVideo, nFrames, minFrameRate);
    
    if firstFrame == -1 % if there are too few video frames
        result = resultNaN(result,i, fullpathVideo, datevideo, FirstProfil,...
                 SecondProfil, ThirdProfil);
        continue
    end
    
    try % catching videos on which the videoReader fails
        [video, frames, img] = loadVideo(fullpathVideo, [], nFrames, firstFrame);
    catch
        video=-1;
    end
    
    if video == -1 % if loadvideo fails     
        result = resultNaN(result,i, fullpathVideo, datevideo, FirstProfil,...
                 SecondProfil, ThirdProfil);
        disp ('Warning : videoreader failed');
        continue
    end
    
    % switch for the tow different cameras
    % after large flood in March 2015 angles of 3.14 (Cam1, instead of 2.7) and 0.5 (Cam2, instead of 3.14) are more appropriate
    if size(img,1)==480  
        
            result(i).Camera = 'Cam1';
            CamCalTsai = Cam1_CalTsai;
            y_line1 = 140; tol1=10; if (i < 9), angle1=2.7; else angle1=3.14; end;
            y_line2 = 270; tol2=10; if (i < 9), angle2=2.7; else angle2=3.14; end;
            y_line3 = 390; tol3=10; if (i < 9), angle3=2.7; else angle3=3.14; end;
            disp('Detected code 00408CB7BDEE for Cam 1')
    end
    
    
    if size(img,1)==576
       
            result(i).Camera = 'Cam2';
            CamCalTsai = Cam2_CalTsai;    
            y_line1 = 140; tol1=10; if (i < 9), angle1=3.14; else angle1=0.5; end;
            y_line2 = 270; tol2=10; if (i < 9), angle2=3.14; else angle2=0.5; end;
            y_line3 = 390; tol3=10; if (i < 9), angle3=3.14; else angle3=0.5; end;
            disp('Detected code 00408CAF9E73 for Cam 2')
            
    end
    
        % get index of the DSM for this camera
        indCam = find( cellfun(@(x)isequal(x,result(i).Camera),{DSMstruct.Camera}));
        % find the closest DSM for this Cam regarding acquisition time
        DSMdates = cellfun(@datenum, {DSMstruct(indCam).Date});
        timeDiff= abs(DSMdates-datenum(datevideo));
        indTime = find(timeDiff == min(timeDiff), 1 );
        
        %select the nearest DSM for this camera
        indDSM = indCam(indTime);
        DSM = struct2array(DSMstruct(indDSM).DSM);
        disp(['Using DSM from ', num2str(DSMstruct(indDSM).Date), ' for ', result(i).Camera])

        [V, Vp] = surfaceVelocity(frames, crop, CamCalTsai, video, framdur, n_im, init_im, doPlot);

        result(i).SurfVelocity(1).V=V;
        result(i).SurfVelocity(1).Vp=Vp;

        flowtype=1; % always 1, others are not implemented anymore
        water_mask = createWatermask(video, frames, V, flowtype,...
                                        firstFrame, nFrames, crop, doPlot);

        aoi = ~isnan(V(:,:,3)); % get mask of the extent of the AOI = area where velocity has been computed

        % watermask are not teste here
        Pr=NaN; Rec=NaN;F=NaN;

        result(i).Date = datevideo;
        result(i).Video = fullpathVideo{:};
        result(i).WaterMask = water_mask;
        result(i).AccWaterMask = [Pr, Rec,F];

        % First profile
        [profil,profilOK] = getProfile(y_line1, angle1, tol1, aoi, water_mask, [], doPlot, img);

        % watermask are not teste here
        Pr=NaN; Rec=NaN;F=NaN;

        if profilOK==1 % only if there are sufficient non-water pixel along the profil 
            [z_water,z_skew, emptybins] = getWaterHeight(profil, DSM, highestFrac, doPlot);
            [Dschrg_tot_median, Dschrg_tot_std, Dschrg_tot_rows] = computeDischargeTsai(Vp,...
             DepthAver, DSM, z_water, profil, angle1, CamCalTsai,...
             doPlot, crop, proj);
        else
            z_water = [];
            z_skew = [];
            emptybins = [];
            Dschrg_tot_median = [];
            Dschrg_tot_std = [];
            Dschrg_tot_rows = [];
        end

        result(i).FirstProfil(1).Profil = profil;
        result(i).FirstProfil(1).WaterHeight = z_water;
        result(i).FirstProfil(1).HeightSkew = z_skew;
        result(i).FirstProfil(1).nEmptybins =emptybins;
        result(i).FirstProfil(1).MedianDischarge = Dschrg_tot_median;
        result(i).FirstProfil(1).TwoSigmaDischarge = Dschrg_tot_std;
        result(i).FirstProfil(1).DischargePerRow = Dschrg_tot_rows;
        result(i).FirstProfil(1).AccWaterMask = [Pr, Rec,F];
        
        % Second profile
        [profil,profilOK] = getProfile(y_line2, angle2, tol2, aoi, water_mask, [], doPlot, img);
    
        % watermask are not teste here
        Pr=NaN; Rec=NaN;F=NaN; 
        
        if profilOK==1 % only if there are sufficient non-water pixel along the profil  
            [z_water,z_skew, emptybins] = getWaterHeight(profil, DSM, highestFrac, doPlot); 
            [Dschrg_tot_median, Dschrg_tot_std, Dschrg_tot_rows] = computeDischargeTsai(Vp,...
             DepthAver, DSM, z_water, profil, angle2, CamCalTsai,...
             doPlot, crop, proj);
        else
            z_water = [];
            z_skew = [];
            emptybins = [];
            Dschrg_tot_median = [];
            Dschrg_tot_std = [];
            Dschrg_tot_rows = [];
        end
        
        result(i).SecondProfil(1).Profil = profil;
        result(i).SecondProfil(1).WaterHeight = z_water;
        result(i).SecondProfil(1).HeightSkew = z_skew;
        result(i).SecondProfil(1).nEmptybins =emptybins;
        result(i).SecondProfil(1).MedianDischarge = Dschrg_tot_median;
        result(i).SecondProfil(1).TwoSigmaDischarge = Dschrg_tot_std;
        result(i).SecondProfil(1).DischargePerRow = Dschrg_tot_rows;
        result(i).SecondProfil(1).AccWaterMask = [Pr, Rec,F];
        
        % Third profile
        [profil,profilOK] = getProfile(y_line3, angle3, tol3, aoi, water_mask, [], doPlot, img);
        
        % watermask are not teste here
        Pr=NaN; Rec=NaN;F=NaN;            

        if profilOK==1 % only if there are sufficient non-water pixel along the profil 
            [z_water,z_skew, emptybins] = getWaterHeight(profil, DSM, highestFrac, doPlot); 
            [Dschrg_tot_median, Dschrg_tot_std, Dschrg_tot_rows] = computeDischargeTsai(Vp,...
             DepthAver, DSM, z_water, profil, angle3, CamCalTsai,...
             doPlot, crop, proj);
        else
            z_water = [];
            z_skew = [];
            emptybins = [];
            Dschrg_tot_median = [];
            Dschrg_tot_std = [];
            Dschrg_tot_rows = [];
        end
        
        result(i).ThirdProfil(1).Profil = profil;
        result(i).ThirdProfil(1).WaterHeight = z_water;
        result(i).ThirdProfil(1).HeightSkew = z_skew;
        result(i).ThirdProfil(1).nEmptybins =emptybins;
        result(i).ThirdProfil(1).MedianDischarge = Dschrg_tot_median;
        result(i).ThirdProfil(1).TwoSigmaDischarge = Dschrg_tot_std;
        result(i).ThirdProfil(1).DischargePerRow = Dschrg_tot_rows;
        result(i).ThirdProfil(1).AccWaterMask = [Pr, Rec,F];
end  

save Output/RefMesTests.mat result
load Output/RefMesTests.mat

 tol=10;
 [dates, DischargeAllRowsPerDate] = extractResults(result, tol);
 DatesAll = cell2mat(dates);
 QAll =  cell2mat(DischargeAllRowsPerDate');
 
dlmwrite('Output/MesDischarge.txt',QAll, ';')
dlmwrite('Output/Dates.txt',  DatesAll, ';')

