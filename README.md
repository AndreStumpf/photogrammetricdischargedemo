This is a demo for photogrammetric discharge measurements described in:

Stumpf, A., E. Augereau, C. Delacourt and J. Bonnier (In Review): Photogrammetric discharge monitoring of small tropical mountain rivers - A case study at Rivière des Pluies, Réunion island. In Review in Water Resources Research.

The repository includes test data sets and scripts to reproduce the results presented in Fig. 8 and Fig. 10 of the paper. It does currently not include tools for the camera calibration and the projection of the topographic data into the image geometry.

If you encounter bugs or if you are interested in obtaining the complete, parallelized code for your own research please contact: andre.stumpf@unistra.fr


# Requirements:

The code was developed and tested with MATLAB R2014a under Ubuntu Linux 14.04.
It should also work under Windows but well, you never know.

The following MATLAB toolboxes might be required:

+ Image Processing Toolbox
+ Statistics Toolbox
+ Symbolic Math Toolbox

You need to install the ffmpeg library which can be obtained from ffmpeg.org.

The calibration and one of the two plots is based on R which can be obtained via https://www.r-project.org/


#Folder contents:
| Folder name | Description                    |
| ------------- | ------------------------------ |
| `/Cameras`      | Videos, camera calibration parameters, reference measurements, and 'ground truth' water masks   |
| `/Functions`   | Matlab functions     |
| `/Output`   | Output measurements and graphics     |
| `/R`   | An R script for calibration and producing Fig. 10    |



#Run the demo:

To reproduce the experiments for testing the accuracy of the water masks open Matlab move to the Demo folder and:

`run('WatermaskTests.m')`

To plot the results use the following within Matlab:

`run('PlotWatermaskTests.m')`

To reproduce the experiments for testing the accuracy of the discharge measurements open Matlab move to the Demo folder and:

`run('ReferenceMeasurements.m')`

To plot the results use the following to call R from the terminal:

`Rscript R/CalVal.R`