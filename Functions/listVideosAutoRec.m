function [pathVideos, fullpathVideos] = listVideosAutoRec()

%listVideos 
%
%   Opens an interactive dialog to select a folder and searches all subfolders
% (avec l'architecture de l'enregistrement automatique) for instances of .mkv 

%   [pathVideos, fullpathVideos] = listVideos();
%
%   Output
%
%   pathVideos: The select path
%
% 
%   fullpathVideos: A list with the full pathes for all videos
%
 
pathVideos = uigetdir(pwd,'Select folder where the videosare located');
 
    i=1;
    liste=listRep(pathVideos); %listing initial
 
    while(1)
        
        if i>size(liste,2) %la boucle s'arrete quand on a atteint la fin de la liste
            break;
        end
 
        if isdir(char(liste(i)))    %si c'est un dossier
            liste=[liste listRep(liste(i))]; %On liste ce dossier et on le colle a la liste deja cree
            liste(i)=[]; %on supprime l'item en cours, c'etait un nom de dossier et on ne voulait que les fichiers
        else
            i=i+1;  %si c'est un fichier, on n'y touche pas et on passe a l'item suivant
        end
        
    end
    
 
    fullpathVideos = {};
    j=1;
    for i=1:size(liste,2)   
        
        % if  strfind(liste{(i)},'.mkv')~=0 %% replaced to search for both
        % asf and mkv
        if ~isempty(regexp(liste{(i)},'.mkv|.asf|.mp4', 'ONCE'))
                                   
             fullpathVideos {j}=liste(i);
             j=j+1;
             
        end
    end
    
    % conversion of MKVs to MP4 when on Linux
    os = computer;
    if strcmp(os, 'GLNXA64')
        
        % check if there is an MKV in the list
        isMKV = cell(size(fullpathVideos,2),1);
        for i=1:size(fullpathVideos,2)   
            if ~isempty(regexp(char(fullpathVideos{i}),'.mkv', 'ONCE'))
                isMKV{i} = fullpathVideos{i};                   
            end
        end
        
        % check if there is already an MP4 with the same name
        isMP4 = cell(size(fullpathVideos,2),1);
        for i=1:size(fullpathVideos,2)   
            if ~isempty(regexp(char(fullpathVideos{i}),'.mp4', 'ONCE'))
                isMP4{i} = fullpathVideos{i};                   
            end
        end
       
       % remove all MKVs for which MP4s are already in the folder
        indMP4 = find(~cellfun(@isempty,isMP4)); 
        isMP4 = [isMP4{indMP4}];
        namesMP4 = cell(size(isMP4,2),1);
        for i=1:size(isMP4,2) 
            [~,nametmp,~] = fileparts(char(isMP4(i)));
            namesMP4{i} = nametmp;
        end
        
        for i=1:size(isMKV,1)
            [~,nameMKV,~] = fileparts(char(isMKV{i}));
            if sum(ismember(namesMP4 ,nameMKV)) == 1
                isMKV{i} = [];
            end
        end
            
        
       indMKV = find(~cellfun(@isempty,isMKV));
       k=0;
       if sum(indMKV ) > 0   
           disp('Matlab video reader does not like MKV format under LINUX. MKVs are beeing converted to MP4 before processing.')
           for i=indMKV'
               disp(fullpathVideos{i})
               [pathstr,name,~] = fileparts(char(fullpathVideos{i}));
               callFFmpeg = strcat('ffmpeg',{' -i '},fullpathVideos{i},{' '},'-map_metadata 0 -vcodec copy -acodec copy -y',{' '}, pathstr,'/', name,'.mp4');
               [~,~] = system(callFFmpeg{:});
               % fullpathVideos{i} =  {strcat(pathstr,'/', name,'.mp4')};
               disp(strcat('Converted to',{' '}, pathstr,'/', name,'.mp4'))
                % disp('You can convert MKV to MP4 with the following command:')
                % disp('for i in *.mkv; do ffmpeg -i $i -map_metadata 0 -vcodec copy -acodec copy ${i%.mkv}.mp4; done');
                k=k+1;
           end
       end
       
       %repeat listing to include only mp4s
       i=1;
       liste=listRep(pathVideos); %listing initial
 
       while(1)
           if i>size(liste,2) %la boucle s'arrete quand on a atteint la fin de la liste
               break;
           end
           if isdir(char(liste(i)))    %si c'est un dossier
               liste=[liste listRep(liste(i))]; %On liste ce dossier et on le colle a la liste deja cree
               liste(i)=[]; %on supprime l'item en cours, c'etait un nom de dossier et on ne voulait que les fichiers
           else
               i=i+1;  %si c'est un fichier, on n'y touche pas et on passe a l'item suivant
           end
       end 
       
       fullpathVideos = {};
       j=1;
       for i=1:size(liste,2)   
            if ~isempty(regexp(liste{(i)},'.mp4|.asf', 'ONCE'))
                 fullpathVideos{j}=liste(i);
                 j=j+1;
            end
        end
   disp([num2str(k), ' videos have been converted to MP4.'])    
   end  
    
disp([num2str(size(fullpathVideos,2)), ' videos have been detected in total.'])



    