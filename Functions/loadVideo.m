function [video, frames, img] = loadVideo(path, name, nFrames, firstFrame, step)

%loadVideo  A wrapper function to read a video prior to correlation or other
%           processing steps
%
%   [video, frames] = loadVideo(path, name)
%   Returns a video object and a matrix of the size nl * nc * 30 where nc is
%   the width of the video, nl is the height of the video and 30 is default
%   value for the number of bands
%   
%   [video, frames, img] = loadVideo(path, name)
%   Same as previous plus additionally returns the first frame as an RGB
%   image
%
%   Input
%   
%   path: Path were the video file is located
%
%   name: Name of the video file, if empty path should contain the complete
%   path
%
%   nFrames[optional]: number of frames to be extracted, 30 by default
%   
%   firstFrame[optional]: Frame at which the extraction will start, 1 by default
%
%   step[optional]: Step size for the extraction of the frames, every n-th frame will
%   be allocated in frames, 1 by default
%
%   Output
%   
%   video: A video object as returned by Matlabs video reader
%   frames: A matrix of the size nl * nc * nFrames where nc is the width of the
%   video, nl is the height of the video
%
%	André Stumpf
%	October 12, 2014


%check input and ouput variables
narginchk(1, 5)
nargoutchk(2, 3) 

if ~exist('firstFrame', 'var') || isempty(firstFrame)
  firstFrame=1;
end

if ~exist('nFrames', 'var') || isempty(nFrames)
  nFrames=30;
end

if ~exist('step', 'var') || isempty(step)
  step=1;
end

if ~exist('name', 'var') || isempty(name)
    path = path{:};
    video = VideoReader(path);
    os = computer;
    if strcmp(os, 'PCWIN64')
        lastFrame = read(video, inf); % avoid bug on windows
    end
else
    video = VideoReader([path{:}, name]);
    os = computer;
    if strcmp(os, 'PCWIN64')
        lastFrame = read(video, inf); % avoid bug on windows
    end
end

if firstFrame + nFrames*step > video.NumberOfFrames
    error('The defined sequence of frames exceeds the length of the video!')
end

if nargout >1
   img = read(video, video.NumberOfFrames-3);
end

vidHeight = video.Height;
vidWidth = video.Width;

% Preallocation to store the video
mov(1:nFrames) = struct('cdata', zeros(vidHeight, vidWidth, 3, 'uint8'),...
    'colormap', []);

% read videos with the defined interval
n=0;
frames=zeros(vidHeight,vidWidth,nFrames, 'uint8'); %ici 100 images max en int (0-255)

for k = 1: step:nFrames
    n=n+1;
    mov(k).cdata = read(video, k+firstFrame);       % read the images into a structure
    im_int=mov(k).cdata;                            % reform the images into a matrix
    frames(:,:,n)=im_int(:,:,1);                    % correlation only on the red band
end


end