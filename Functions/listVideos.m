function [pathVideos, dirinfo, subdirinfo, fullpathVideos] = listVideos()

%listVideos 
%
%   Opens an interactive dialog to select a folder and searches all subfolders
% (only one level below) for instances of .asf and .mp4
%
%   [pathVideos, dirinfo, subdirinfo, fullpathVideos] = listVideos();
%
%   Output
%
%   pathVideos: The select path
%
%   dirinfo: List of all contained directories
%   
%   subdirinfo: List with all videos per subdirectory
%
%   fullpathVideos: A list with the full pathes for all videos
%
%	André Stumpf
%	January 21, 2015
                  
%check output variables
nargoutchk(1, 4)

    pathVideos = uigetdir(pwd,'Select folder where the videos are located');
    dirinfo = dir(pathVideos);
    dirinfo(~[dirinfo.isdir]) = [];  %remove non-directories

    subdirinfo = cell(length(dirinfo),1);
    for K = 1 : length(dirinfo)
          if (sum(strcmp(dirinfo(K).name, {'.','..'})) == 0) % skip dynamic links  
              thisdir = dirinfo(K).name;
              os = computer;
              if strcmp(os, 'PCWIN64')
                  subdirinfo{K} = [dir(fullfile(pathVideos,thisdir, '*.asf')); dir(fullfile(pathVideos,thisdir, '*.mkv'))];
              end
              if strcmp(os, 'GLNXA64')
                  subdirinfo{K} = [dir(fullfile(pathVideos,thisdir, '*.asf')); dir(fullfile(pathVideos,thisdir, '*.mp4'))];
              end   
         else continue

         end
    end
    
    if strcmp(os, 'GLNXA64')
       disp('Matlab video reader does not like MKV format under LINUX. MP4 has been searched instead.')
       disp('You can convert MKV to MP4 with the following command:')
       %disp('for i in *.mkv; do ffmpeg -i $i -vcodec copy -acodec copy ${i%.mkv}.mp4; done');
       disp('for i in *.mkv; do ffmpeg -i $i -map_metadata 0 -vcodec copy -acodec copy ${i%.mkv}.mp4; done');
       %disp('for i in *.mkv; do ffmpeg -i $i -map_metadata 0:s:0 -vcodec copy -acodec copy ${i%.mkv}.mp4; done');
    end  

    fullpathVideos ={};
    k=0;
    for i = 1 : length(subdirinfo)
        if (sum(strcmp(dirinfo(i).name, {'.','..'})) == 0) % if false skip dynamic links
            try
                videoNameTemp = extractfield(subdirinfo{i}, 'name');
            catch
                videoNameTemp =[];
                warning(['No video found in directory ', dirinfo(i).name])
                disp('Directory might be empty or contain a none recognized format')
            end        
            for j = 1 : length(videoNameTemp)
                k=k+1;
                fullpathVideos{k} = fullfile(pathVideos,dirinfo(i).name,videoNameTemp(j));
            end
          else continue
        end
    end    
disp([num2str(k), ' videos have been detected.'])
    
end
    