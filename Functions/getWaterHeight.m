function [z_water,z_skew, emptybins] = getWaterHeight(profil, DSM_image, highestFrac, doPlot)


%check input arguments
narginchk(2, 4)

if ~exist('doPlot', 'var'), doPlot=0; end
if ~exist('highestFrac', 'var'), highestFrac=0.05; end

z_Profil = DSM_image(profil);

emptybins = 0;
% check histogramm for empty bins
binwidth = (max(z_Profil(:)) - min(z_Profil(:)))/30;
binstart = min(z_Profil(:));
binend = max(z_Profil(:));
binranges = binstart:binwidth:binend;
bincounts = histc(z_Profil(:),binranges);
if sum(bincounts==0)>2
    startend = diff(bincounts==0);
    binnumber = find(startend == 1);
    maxbin = binstart + binnumber*binwidth;
    maxbin = min(maxbin); % given the case that there are multiple empty bins take the bin with the smaller value
    z_Profil(z_Profil>maxbin)=[];
    emptybins = sum(bincounts==0);
end

z_skew = skewness(z_Profil);
   
%    % supress highest bins until skewness drops below 1
%    while z_skew > 1.0
%      [~,centers] = hist(z_Profil(:),30);
%       z_Profil(z_Profil > centers(end)) =[]; 
%       z_skew = skewness(z_Profil);
%    end 

 if  z_skew > 1.0
%    % fit normal distribution and take the mean + 2 sigma
%     pd = fitdist(z_Profil(:),'Normal');
%     z_Profil(z_Profil > (pd.mu + pd.sigma)) = [];
    
%     % inner quartile statistics 
%     iq_stdv = iqr(z_Profil(:)) * 0.7413; 
%     iq_tmean = trimmean(z_Profil(:),25);
%     z_water = iq_tmean + 2.0* iq_stdv;
    
     % median absolute deviation
%     mad_z = mad(z_Profil,1);
%     sigma_z = 1.4826*mad_z;
%     med_z = nanmedian(z_Profil);
%     z_water = med_z + 2.5*sigma_z;

    % Qn after Rousseeuw & Croux 1993
    %sn(z_Profil)
    qn_z =  qn(z_Profil);
    med_z = nanmedian(z_Profil);
    z_water = med_z + 3*qn_z;
    
    %   % ROSIN THRESHOLD    
%      h = hist(z_Profil(:),30);
%      [counts,centers] = hist(z_Profil(:),30);
%      T = RosinThreshold(counts, 1)
%      centers(T)
 
%   % T Point threshold
%     [counts,centers] = hist(z_Profil(:),30);
%     cMax = find(counts==max(counts),1,'first');
%     counts2 = counts(cMax:end);
%     centers2 = centers(cMax:end);
%     TPoint=TPointMethod(counts2,centers2,1)
%     figure; plot(centers,counts), hold on, plot([centers2(TPoint) centers2(TPoint)],[0 max(counts)],'r')
    
else    
    % take the median of the upper tail as the height value
    z_Profil_sort = sort(z_Profil, 'descend');
    z_Profil_sort(isnan(z_Profil_sort)) = [];
    z_Profil_sort = z_Profil_sort(1:round(size(z_Profil_sort,1)*highestFrac));
    z_water = nanmedian(z_Profil_sort);
end

if doPlot==1
    
    bb = regionprops(profil,'BoundingBox');
    bb = struct2array(bb);
    xmin = min(bb(1:4:end))-20;
    xstep= bb(3:4:end);
    xmax = max(bb(1:4:end))+xstep(end)+20;
    ymin = min(bb(2:4:end))-20;
    ystep = max(bb(4:4:end));
    ymax = max(bb(2:4:end))+ystep+20;
    
%     if length(bb)>1
%         minbb = min(bb.BoundingBox);
%         maxbb = max(bb.BoundingBox);
%         xmin = minbb(1)-20;
%         xmax = maxbb(1)+maxbb(3)+20;
%         ymin = minbb(2)-20;
%         ymax = maxbb(2)+maxbb(4)+20;
%     else   
%         bb=cat(1,bb.BoundingBox);
%         xmin = bb(1)-20;
%         xmax = bb(1)+bb(3)+20;
%         ymin = bb(2)-20;
%         ymax = bb(2)+bb(4)+20;
%     end
    
    profil_plot = double(profil);
    
    subplot(2,2,1);
    imagesc(DSM_image(:,:,1));
    green = cat(3, zeros(size( profil_plot)), ones(size( profil_plot)),...
        zeros(size( profil_plot)));
    hold on
    h = imshow(green);
    hold off
    profil_plot(profil_plot==1) = 0.3;
    set(h, 'AlphaData',  profil_plot)
    axis([xmin, xmax, ymin, ymax])
    title('Profil on DSM');
    cmin = min(min(DSM_image(:,:,1)));
    cmax = max(max(DSM_image(:,:,1)));
    caxis([cmin cmax])
    colormap('default')
    colorbar
    freezeColors   
    
%     subplot(2,2,2);
%     imagesc(DSM_image(:,:,2))    
%     axis([xmin, xmax, ymin, ymax])
%     axis equal
%     colormap('gray')
%     %colorbar
%     title('Greeness - not used at the moment');
%     freezeColors
%     
%     subplot(2,2,3);
%     imagesc(DSM_image(:,:,3))    
%     axis([xmin, xmax, ymin, ymax])
%     axis equal
%     colormap('gray')
%     %colorbar
%     title({'Correlation coefficient for DTM', 'Not used at the moment'});
%     freezeColors
    
    subplot(2,2,4);
    hist(z_Profil, 30);
    yL = get(gca,'YLim');
    line([z_water z_water],yL,'Color','r');
    title('Estimate of the water height')
    colormap('default')
    xax = max(get(gca,'xlim'));
    yax = max(get(gca,'ylim')*0.7);
    text(xax, yax,  {['Z water= ', num2str(z_water,4), ' m'],...
            'Relative to mean' , 'GCP height'}, 'FontSize',10,...
            'HorizontalAlignment', 'right')
        
    subplot(2,2,1);
    imagesc(DSM_image(:,:,1));
    green = cat(3, zeros(size( profil_plot)), ones(size( profil_plot)),...
        zeros(size( profil_plot)));
    hold on
    h = imshow(green);
    hold off
    profil_plot( profil_plot==1) = 0.3;
    set(h, 'AlphaData',  profil_plot)
    axis([xmin, xmax, ymin, ymax])
    title('Profil on DSM');
    cmin = min(min(DSM_image(:,:,1)));
    cmax = max(max(DSM_image(:,:,1)));
    caxis([cmin cmax])
    colormap('default')
    colorbar

end