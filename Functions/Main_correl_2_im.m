
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cette version est utilisable lorsque les images sont dans une geometrie
% voisine (images pseudo orthorectifies). 
% Le programme corr�le deux fen�tres autour d'un point sans tenir
% compte des alt�rations g�om�triques produites par les diff�rences
% d'orientation entre cam�ra.
% ATTENTION !! il faut modifier cpcorr en pla�ant un global devant le CORRSIZE
% et le THRESHOLD. Il faut aussi supprimer leur valeur dans cette fonction

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ipt_ok bpt_ok]=Main_correl_2_im(base, input)

global CORRSIZE THRESHOLD pasxety pos_transect


[nl_base nc_base]=size(base(:,:,1));
[nl_input nc_input]=size(input(:,:,1));

grille_tot=zeros(nl_input, nc_input);
%%%%%%%%%%%%%%%%%%%%
%fabrication de la grille


[pos_input, grille_tot]=fabri_grille3(pasxety,grille_tot);

pos_base=pos_input;
n=max(max(size(pos_base)));
%%%%%%%%%%%%%%%%%%
%tri des points: on cherche les points qui sont dans l'image (inutile si
%les images sont identiques)
[pos_base, pos_input]=tri_grille3(pos_base, pos_input, nc_input, nl_input);

%%%%%%%%%%%%%%%%%%
%tri des points: on cherche les points qui ne sont pas ds le noir....

az=zeros(length(pos_input(:,1)),1);
for i=1:length(pos_input(:,1))
    az(i,1)=input(pos_input(i,2),pos_input(i,1));
end
res=find(az~=0);
pos_input_2=zeros(length(res),2);
pos_base_2=zeros(length(res),2);
pos_input_2(:,1)=pos_input(res,1);
pos_input_2(:,2)=pos_input(res,2);
pos_base_2(:,1)=pos_base(res,1);
pos_base_2(:,2)=pos_base(res,2);
pos_base=pos_base_2;
pos_input=pos_input_2;
clear pos_input_2 pos_base_2

length(pos_input(:,1));

%figure(1); imagesc((base));axis ij; hold on; colormap 'gray'; plot(pos_base(:,1),pos_base(:,2),'.')
%figure(2); imagesc(input);axis ij; hold on; colormap 'gray'; plot(pos_input(:,1),pos_input(:,2),'.')


ipt=pos_input;
ipt2=ipt;
bpt=pos_base;

longX=length(bpt);
 

% if length(ipt2)>10000
%     %%%%%%%%%%%%%%%%%%%%%%%%%
%     % calcul de l'heure de fin de calcul
%     t = clock;
%     iuiuiu=cpcorr_mod(ipt2(1:1000,:),pos_base(1:1000,:),input(:,:,1),base(:,:,1));
%     cde=etime(clock, t);
%     nb_de_seconde=longX/1000*cde;
%     nb_heures=floor(nb_de_seconde/3600)
%     reste=nb_de_seconde-nb_heures*3600;
%     nb_minutes=floor(reste/60)
%     clear iuiuiu
% end

pas_2=200000;
for i=1:pas_2:longX
  if (i+pas_2-1)<longX
   but=(i+pas_2-1);
else
   but=longX;
end

j=i:but;


ipt2(j,:) = cpcorr_mod(ipt2(j,:),pos_base(j,:),input(:,:,1),base(:,:,1));


end


ipt_adj=ipt2;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%d�tection des points corrects%%
nptsc=max(max(size(ipt2)));
[ipt_ok, bpt_ok]=point_correct(ipt,ipt_adj,bpt);
nptsc_ok=max(max(size(ipt_ok)));


