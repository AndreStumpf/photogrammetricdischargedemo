function [Dschrg_tot_median, Dschrg_tot_std, Dschrg_tot_rows] = computeDischargeTsai(Vp,...
        DepthAver, DSM_image, z_water, profil, angle,  Cam_CalTsai, doPlot,...
        crop, proj)

if ~exist('doPlot', 'var'), doPlot=0; end
if ~exist('proj', 'var'), proj=1; end

if proj==1
    disp('Projecting velocities normal to profil...')
    % projection of the velocities normal to the profil
    pn.xy = [sin(-angle), cos(-angle)];

    for i=1:1:size(Vp(:,1))
        Vp(i,3:4) = dot(Vp(i,3:4),pn.xy)/norm(pn.xy)*pn.xy;
    end
end    

disp('Computing discharge...')
% matrix for reprojection
nc = Cam_CalTsai{1};
nl = Cam_CalTsai{2};
[X_im, Y_im]=meshgrid(1:1:nc,1:1:nl);

% compute ground coordinates of the the pixel according to estimated water
% height
Pts2D=[X_im(:) Y_im(:)];
PtsZ = ones(length(Pts2D),1)*z_water ; % Z = 0;
Pts3DProj = Image2GroundTsai(Pts2D, PtsZ, Cam_CalTsai);

% compute resolution at the ground
X_sol_mat=reshape(Pts3DProj(:,1), nl, nc);
Y_sol_mat=reshape(Pts3DProj(:,2), nl, nc);
resol_x=diff(X_sol_mat);
resol_y=diff(Y_sol_mat);

% conversion factor for the duration of on frame
% pas_vid = video.Duration / video.NumberOfFrames;
% pas_vid = 1/video.FrameRate;
% this will lead to an overestimation in the case that the video is bucking

pas_vid = 1; % current implementation with surfaceVelocity3 provides pixel/s directly

% account for the offset due to the cropped area
Vp(:,1) = Vp(:,1) + crop(1);
Vp(:,2) = Vp(:,2) + crop(2);

VpGround=Vp; % make a copy of the same size to store outputs

% compute the ground velocities as image velocity * ground resolution / time step
for i=1:1:size(VpGround(:,1)) %for all points
    y_im=VpGround(i,1);       % image coordinates
    x_im=VpGround(i,2);
    VpGround(i,3)=-resol_x(x_im,y_im)*Vp(i,3)/pas_vid; % ground resolution(for image x image y) * displacement / time step
    VpGround(i,4)=resol_y(x_im,y_im)*Vp(i,4)/pas_vid; 
end

   
% compute depth averaged velocities
U_da=VpGround(:,3) * DepthAver;
V_da=VpGround(:,4) * DepthAver;

% Interpolation of the measured vectors (ground/m) to the resolution of the image
v_tot_da = griddata(VpGround(:,1), VpGround(:,2), (V_da.^2+U_da.^2).^(0.5), X_im, Y_im, 'natural');

% orient all layers so that the profile runs horizontal
angle_deg = radtodeg(angle-pi);
profil_h = imrotate(profil, angle_deg, 'bilinear');
DSM_image_h = imrotate(DSM_image(:,:,1), angle_deg, 'bilinear');
v_tot_h =  imrotate(v_tot_da, angle_deg, 'bilinear');
X_sol_mat_h = imrotate(X_sol_mat, angle_deg, 'bilinear');
Y_sol_mat_h = imrotate(Y_sol_mat, angle_deg, 'bilinear');

% subset them to the bounding box of the profile
profile_bounds = regionprops(profil_h, 'Extrema');
profile_bounds =  struct2array(profile_bounds);
ymin_profil = min(ceil(profile_bounds(1,2:2:end)));
ymax_profil = max(floor(profile_bounds(5,2:2:end)));
xmin_profil = min(ceil(profile_bounds(7,1:2:end)));
xmax_profil = max(floor(profile_bounds(3,1:2:end)));

% profile_bounds = regionprops(profil_h, 'Extrema');
% profile_bounds =  struct2array(profile_bounds);
% ymin_profil = ceil(profile_bounds(1,2));
% ymax_profil = floor(profile_bounds(5,2));
% xmin_profil = ceil(profile_bounds(7,1));
% xmax_profil = floor(profile_bounds(3,1));

DSM_image_profil = DSM_image_h(ymin_profil:ymax_profil,xmin_profil:xmax_profil);
Q_tot_sol_profil = v_tot_h(ymin_profil:ymax_profil,xmin_profil:xmax_profil);
X_sol_profil = X_sol_mat_h(ymin_profil:ymax_profil,xmin_profil:xmax_profil);
Y_sol_profil = Y_sol_mat_h(ymin_profil:ymax_profil,xmin_profil:xmax_profil);
profil_sub = profil_h(ymin_profil:ymax_profil,xmin_profil:xmax_profil);

% compute distances among the pixels
delta_x = diff(X_sol_profil,1,2);
delta_y = diff(Y_sol_profil,1,2);
delta_xy = sqrt(delta_x.^2 + delta_y.^2);
delta_xy = [delta_xy, delta_xy(:,end)]; 

% There is some aproximation here. Concatenating the pixel size of the last
% is not 100% correct since is will be slight different. It is in general
% not clear if the projected coordinates mark the center of the pixel.
% However, the effect is probably neglible.

% set velocity to 0 where DSM above z_water
Q_tot_sol_profil(DSM_image_profil > z_water) = 0;
% set velocity to 0 outside the water mask
Q_tot_sol_profil(~profil_sub) = 0;

% length along the profile for the x-axis of the graph
profil_mid = ceil(size(profil_sub,1)/2);
dist_xy_mid = cumsum(delta_xy(profil_mid,:));

% compute for each pixel in the profile Discharage = depth * width * velocity
Dschrg_profil = (z_water - DSM_image_profil) .* delta_xy .* Q_tot_sol_profil;

% sum along each profil line
Dschrg_tot_rows = nansum(Dschrg_profil,2);

% median of all profil lines and standard deviation
Dschrg_tot_median = nanmedian(Dschrg_tot_rows);
Dschrg_tot_std = 2*qn(Dschrg_tot_rows);  

% This is a first order estimate of the uncertainty which only partially
% accounts for errors in the projection, velocity measurments or terrain
% model

% % allocate result vectors for visualization
% Q_mean_col = zeros(size(profil_sub,2),1);
% Q_std_col = zeros(size(profil_sub,2),1);
% DSM_mean_col = zeros(size(profil_sub,2),1);
% DSM_std_col = zeros(size(profil_sub,2),1);
% Dschrg_mean_col = zeros(size(profil_sub,2),1);
% Dschrg_std_col = zeros(size(profil_sub,2),1);
% 
% %compute means and standard deviations along the column
% for i = 1: size(profil_sub, 2)
%    profil_col = profil_sub(:,i);
%    Dschrg_mean_col(i,1) = nanmean(Dschrg_profil(profil_col,i));
%    Dschrg_std_col(i,1) = nanstd(Dschrg_profil(profil_col,i));
%    Q_mean_col(i,1) = nanmean(Q_tot_sol_profil(profil_col,i))/DepthAver; % convert back to surface velocities
%    Q_std_col(i,1) = nanstd(Q_tot_sol_profil(profil_col,i))/DepthAver;   % convert back to surface velocities
%    DSM_mean_col(i,1) = nanmean(DSM_image_profil(profil_col,i));
%    DSM_std_col(i,1) = nanstd(DSM_image_profil(profil_col,i));
% end

Dschrg_mean_col = nanmean(Dschrg_profil,1)';
Dschrg_std_col = nanstd(Dschrg_profil,1)';
Q_mean_col = nanmean(Q_tot_sol_profil,1)/DepthAver; % convert back to surface velocities
Q_std_col = nanstd(Q_tot_sol_profil,1)/DepthAver; % convert back to surface velocities
DSM_mean_col = nanmedian(DSM_image_profil,1);
DSM_std_col = nanstd(DSM_image_profil,1);

if doPlot == 1
    figure;hold on;
    set(gcf,'color','w')
    H1 = shadedErrorBar(dist_xy_mid, Q_mean_col, Q_std_col,... 
        {'-b', 'LineWidth', 1.5}, 0.5);
    H2 = shadedErrorBar(dist_xy_mid,  DSM_mean_col-z_water, DSM_std_col,...
        {'k', 'LineWidth', 1}, 1.5);

    line(get(gca,'xlim'), [0 0], 'LineStyle', '--');

    min_xaxis = min(DSM_mean_col(:)) - max(DSM_std_col(:))-z_water-0.5;
    axis([min(dist_xy_mid), max(dist_xy_mid) min_xaxis 5])

    xlabel('Distance along profile [m]') % x-axis label
    ylabel('Riverbed depth [m]       /      Surface velocity [m/s] ') % y-axis label

    ax2 = axes('XAxisLocation','top','YAxisLocation','right','XLim', [min(dist_xy_mid),max(dist_xy_mid)],...
        'YLim', [0, max(Dschrg_mean_col(:))*2],...
        'Color','none', 'YColor','r');
    % ylabel(ax2,'Discharge [m^3/s] ') % y-axis label
    % set(ax2,'YAxisLocation', 'right')

    Qline  = line(dist_xy_mid, Dschrg_mean_col,'Parent',ax2,'Color','r',...
        'LineWidth', 1.5);

    %Calculate the error bars
    uE= Dschrg_mean_col + Dschrg_std_col;
    lE= Dschrg_mean_col - Dschrg_std_col;
    %Make the patch
    yP=[fliplr(lE'),uE'];
    xP=[fliplr(dist_xy_mid), dist_xy_mid];
    %remove nans otherwise patch won't work
    xP(isnan(yP))=[];
    yP(isnan(yP))=[];

    patchColor = 'r';
    faceAlpha = 0.15;
    edgeColor = 'r';    
    H.patch=patch(xP,yP,1,'facecolor',patchColor,...
                  'edgecolor',edgeColor,...
                  'facealpha',faceAlpha, 'Parent',ax2);

    legend([H1.mainLine, H1.patch H2.mainLine, H2.patch, Qline H.patch], ...
        '\mu Surface velocity', '1\sigma', '\mu Riverbed', '1\sigma',...
        '\mu Discharge', '1\sigma',...
        'Location', 'Northwest');  

    title(['Total discharge = ', num2str(Dschrg_tot_median,2), ' \pm ',...
        num2str(Dschrg_tot_std,2), ' m³ /s'], 'FontSize', 16)

    hold off;
end
                                        
% imagesc(DSM_image_profil)
% clf
%     imagesc(DSM_image_profil)
%     hold on
%     h = imagesc(profil_sub); % show the edge image
%     hold off
%     set( h, 'AlphaData', .3 );
%     title('Watermask');
%     
%     imagesc(DSM_image_h)
%     hold on
%     h = imagesc(profil_h); % show the edge image
%     hold off
%     set( h, 'AlphaData', .3 );
%     title('Watermask');

end