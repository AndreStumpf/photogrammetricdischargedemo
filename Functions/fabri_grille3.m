function [pos_base, grille_tot]=fabri_grille3(pasxety,grille_tot);
dec=10;
[nl_input,nc_input]=size(grille_tot);
grille_int=zeros(nl_input,nc_input);
for i=dec:pasxety:nl_input-dec
    for j=dec:pasxety:nc_input-dec
        grille_int(i,j)=1;
    end
end
grille=abs(grille_tot-grille_int);
res=find(grille==1);

res_col=floor(res/nl_input)+1;
res_lig=res-(res_col-1)*nl_input;

pos_base=[res_col, res_lig];
grille_tot=grille;

