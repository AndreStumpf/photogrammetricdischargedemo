function [ipt_ok, bpt_ok]=point_correct(ipt,ipt_adj,bpt)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%détection des points corrects%%
nptsc=max(max(size(ipt_adj)));
interm=zeros(nptsc,1);
interm=ipt_adj(:,1)-ipt(:,1);
res=find(interm~=0);
ipt_ok=ipt_adj(res,:);
bpt_ok=bpt(res,:);

% assignin('base','ipt_ok',ipt_ok);
% assignin('base','bpt_ok',bpt_ok);

nptsc_ok=max(max(size(ipt_ok)));
