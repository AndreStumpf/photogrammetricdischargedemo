function [firstFrame,framdur,duration,datevideo] = getFrameInfos(fullpathVideo, nFrames, minFrameRate)
    
%getFrameInfos
% This function probes a video stream an extracts the section of a
% specified length (nFrames) with the highest frame rates.
% This is especially important since most surveillance and web cameras
% do not provide constant frame rates. Without correcting for the
% variability in the framerate bucking videos will lead to an
% overestimation of the surface velocities.

%minFrameRate: The minimum acceptable frame rate
%nFrames: Desired number of frames

% It also searches for the creation time of the video within the
% metadata. WARNING!!! If there is not entry for the creation date in
% the metadata the last modification date of the file will be assumed
% as the creation date. This will lead to false dating of the
% measurements if the modification date does not correpond to the time
% of the video acquistion.

% framdur returns the time passed between recording of the frame i and
% the frame i+1, hence it's one element short than nFrames

% make sure that ffprob is installed
% on Ubuntu: sudo apt-get install ffprobe
       
    % sytem call to ffprope
    modifiedStr = strrep(fullpathVideo, ' ', '\ ');
    callFFprobe = strcat('ffprobe',{' '},modifiedStr,{' '},'-print_format csv -show_entries packet=pts_time,dts_time');
    [status,frame_timestamps] = system(callFFprobe{:});
    
    %check if probing was succesfull
    if status ~= 0
        warning('Not able to probe the specified video. Check ffprobe or video path')
        firstFrame = -1;
        framdur = -1;
        duration = -1;
        datevideo = NaN;
        return
    end
    
    %check for the first occurence of 'packet' and get the creation date
    % for windows, the first occurence 'packet' is the first line
    os = computer;
    firstRead = textscan(frame_timestamps,'%s', 'delimiter', '\n');
    if strcmp(os, 'PCWIN64')%(windows)
        startInd=1;
        
    else  %(linux)
        startInd=strfind(firstRead{:} ,'packet');
        startInd=cellfun(@isempty,startInd);
        startInd = find(startInd == 1, 1, 'last' );
    end
    
    dateInd=strfind(firstRead{:} ,'creation_time');
    dateInd = find(~cellfun(@isempty,dateInd), 1 );
    % if creation date can't be found use the last modification date
    if isempty(dateInd)             
        fileInfo = dir(fullpathVideo{:});
        % check we got a single entry corresponding to the file
        assert(numel(fileInfo) == 1, 'No such file: %s', fullpathVideo{:});
        datevideo = datevec(fileInfo.datenum);
    else
        firstRead = firstRead{:};
        datevideo = firstRead{dateInd};
        datevideo = strrep(datevideo,'creation_time   : ','');
        datevideo = datenum(datevideo, 'yyyy-mm-dd hh:MM:ss');
        datevideo = datevec(datevideo);
    end
    
    %extract timestamps from string
    frame_timestamps_temp = strrep(frame_timestamps, 'N/A', '');
    frame_timestamps_temp = textscan(frame_timestamps_temp,'%*s %*s %f', 'HeaderLines',startInd,'Delimiter',',');
    frame_timestamps_temp = cell2mat(frame_timestamps_temp);
    
    % compute time steps between each frame
    frame_duration = diff(frame_timestamps_temp);
    
    % catch the case of videos which are too short of very low frame rate
    if (length(frame_duration)-length(find(isnan(frame_duration))))<=nFrames || mean(frame_duration)>minFrameRate 
        
        disp('The number of requested frames exceeds the length of the video')
        disp(['or the frame rate is below ', num2str(minFrameRate), ' Hz'])
        [~, name]=fileparts(fullpathVideo{:});
        disp(['Video ', name, ' will not be processed'])
        firstFrame = -1;
        framdur = -1;
        duration = -1;
        return
    else    
        % compute the rolling sum over nframes and find the subset with the
        % fastest frame rate/shortest duration
        cum_frame_duration = conv(frame_duration,ones(nFrames,1), 'same');    
        nframes_before = floor(nFrames/2);
        nframes_after = ceil(nFrames/2)-1;
        cum_frame_duration(1:nframes_before) = NaN; % remove values computed with zero padding
        cum_frame_duration(end-nframes_after:end) = NaN; % remove values computed with zero padding
        [duration, max_ind] = nanmin(cum_frame_duration);
        
        if isnan(duration)
            disp('Not enough frames')
            disp(['or the frame rate is below ', num2str(minFrameRate), ' Hz'])
            [~, name]=fileparts(fullpathVideo{:});
            disp(['Video ', name, ' will not be processed'])
            firstFrame = -1;
            framdur = -1;
            duration = -1;
            return
        end 
        
        % get the time step between frame i and i+1
        firstFrame = max_ind-nframes_before;
        framdur = frame_duration(firstFrame:firstFrame+nFrames-2);
    end
end