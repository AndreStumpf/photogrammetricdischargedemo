function result = resultNaN(result,i, fullpathVideo, datevideo, FirstProfil,...
    SecondProfil, ThirdProfil) 

%%resultNaN  Convenience function which sets all variables in the result
%%structure to NaN.
%
%   result = resultNaN(result,i,result,i, fullpathVideo, datevideo, FirstProfil,...
%   SecondProfil, ThirdProfil)
%   Takes the the result structure defined for the batch processing of
%   videos and sets all elements for the current iteration i to NaN. All
%   input arguments are mandatory
%
%	André Stumpf
%	Februar 06, 2015
                  
%check input variables
narginchk(7, 7)
nargoutchk(1, 1)

Pr=NaN; Rec=NaN;F=NaN;
result(i).Date = datevideo;
result(i).Camera = NaN;
result(i).Video = fullpathVideo{:};
result(i).WaterMask = NaN;
result(i).AccWaterMask = [NaN,NaN,NaN];
result(i).FirstProfil = FirstProfil;
result(i).FirstProfil(1).Profil = NaN;
result(i).FirstProfil(1).WaterHeight = NaN;
result(i).FirstProfil(1).HeightSkew = NaN;
result(i).FirstProfil(1).nEmptybins = NaN;
result(i).FirstProfil(1).MedianDischarge = NaN;
result(i).FirstProfil(1).TwoSigmaDischarge = NaN;
result(i).FirstProfil(1).DischargePerRow = NaN;
result(i).FirstProfil(1).AccWaterMask = [NaN,NaN,NaN];
result(i).SecondProfil = SecondProfil;
result(i).SecondProfil(1).Profil = NaN;
result(i).SecondProfil(1).WaterHeight = NaN;
result(i).SecondProfil(1).HeightSkew = NaN;
result(i).SecondProfil(1).nEmptybins = NaN;
result(i).SecondProfil(1).MedianDischarge = NaN;
result(i).SecondProfil(1).TwoSigmaDischarge = NaN;
result(i).SecondProfil(1).DischargePerRow = NaN;
result(i).SecondProfil(1).AccWaterMask = [NaN,NaN,NaN];
result(i).ThirdProfil = ThirdProfil;
result(i).ThirdProfil(1).Profil = NaN;
result(i).ThirdProfil(1).WaterHeight = NaN;
result(i).ThirdProfil(1).HeightSkew = NaN;
result(i).ThirdProfil(1).nEmptybins =NaN;
result(i).ThirdProfil(1).MedianDischarge = NaN;
result(i).ThirdProfil(1).TwoSigmaDischarge = NaN;
result(i).ThirdProfil(1).DischargePerRow = NaN;
result(i).ThirdProfil(1).AccWaterMask = [Pr, Rec,F];

end