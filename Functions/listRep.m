function liste=listRep(pathVideos)

os = computer;
if strcmp(os, 'PCWIN64')
   if ~strcmp(pathVideos(end),'\') % Test if there is a '\' at the end of the path
        pathVideos = strcat(pathVideos,'\');
   end
end
if strcmp(os, 'GLNXA64')
    if ~strcmp(pathVideos(end),'/') % Test if there is a '/' at the end of the path
        pathVideos = strcat(pathVideos,'/');
    end
end



 
pathVideos=char(pathVideos);
 
fichiers=dir(pathVideos);%r�cup�ration des fichiers/dossiers situ�s dans chemin_courant
fichiers=fichiers(3:end);%suppression des dossiers [.] et [..] (racine et dossier parent si mes souvenirs sont bons)
 
liste=([]);
for i=1:size(fichiers,1)
    
    liste{i}=strcat(pathVideos,fichiers(i).name); %enregistrement des fichiers dans une structure, pour �viter les problemes de taille des strings.

end