function[dates, DischargeAllRowsPerDate, camera, percWater] = extractResults(result, tol, nstartDates, ...
                                            dates, DischargeAllRowsPerDate, camera)

    nVideos = length(result);

    %extract values from the structures
    %get dates
    if ~exist('dates', 'var')
        dates = cell(nVideos,1);
    end
    
    if ~exist('nstartDates', 'var')
        nstartDates = 1;
    end

    for i=1:nVideos
        if isempty(result(i).Date)
            continue
        else
            dates{nstartDates-1+i} = result(i).Date;
        end
    end
    
    %get camera name
    if ~exist('camera', 'var')
        camera = cell(nVideos,1);
    end
    
    if ~exist('nstartDates', 'var')
        nstartDates = 1;
    end
    
    %get percentage of water in the ROI
    if ~exist('percWater', 'var')
        percWater = cell(nVideos,1);
    end
    
    for i=1:nVideos
        if ~isfield(result(i), 'percWater')
            continue
        end
        
        if isempty(result(i).percWater)
            continue
        else
            percWater{nstartDates-1+i} = result(i).percWater;
        end
    end
    
    for i=1:nVideos
        if isempty(result(i).Camera)
            continue
        else
           camera{nstartDates-1+i} = result(i).Camera;
        end
    end


    nRowProfil = 4*tol+2; % maximum number of rows per profil
    if ~exist('DischargeAllRowsPerDate', 'var')
        DischargeAllRowsPerDate = cell(length(dates),1);
    end

    for i=1:nVideos

        DischargeAllRowsPerDate_temp =  NaN * ones(3*nRowProfil,1);
        rowStart=1; % start index

        if ~isempty(result(i).FirstProfil)
            nRows = length(result(i).FirstProfil.DischargePerRow);
            rowEnd = rowStart + nRows-1;           
            DischargeAllRowsPerDate_temp(rowStart:rowEnd) = ...
            result(i).FirstProfil.DischargePerRow;
        end

        rowStart = rowStart+nRowProfil;
        if ~isempty(result(i).SecondProfil)
            nRows = length(result(i).SecondProfil.DischargePerRow);
            rowEnd = rowStart + nRows-1;            
            DischargeAllRowsPerDate_temp(rowStart:rowEnd) = ...
            result(i).SecondProfil.DischargePerRow;            
        end

        rowStart = rowStart+nRowProfil;
        if ~isempty(result(i).ThirdProfil)
            nRows = length(result(i).ThirdProfil.DischargePerRow);
            rowEnd = rowStart + nRows-1; 
            DischargeAllRowsPerDate_temp(rowStart:rowEnd) =....
            result(i).ThirdProfil.DischargePerRow;
        end

        DischargeAllRowsPerDate{(length(dates)-nVideos+i)} = DischargeAllRowsPerDate_temp;

    end
end

