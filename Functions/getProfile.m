function [profil,profilOK] = getProfile(y_line, angle, tol, aoi, water_mask, doCheck,...
                               doPlot, img)
%%getProfile
%
% The function returns the intersection of a specified profil with the
% input water mask. In addition it verifies if there are sufficient pixel
% (20) which are not covered by water
%
% Input
%
% y_line: Position of the profile in pixel in Y direction counting from the
%         top of the image. X is automatically set to half the image width.
%
% angle: Angle of the profile around its XY center
%
% tol: Distance of the considered pixels measured in pixel perpendicular 
%      to the profil
%
% aoi: Subset of the image which is considered in the analysis. Typically
%      depends on the output of the surfaceVelocity function.
%
% water_mask: Binary mask marking pixels covered by the river.
%
% doCheck: If set to 1 a dialog will open which allows manual editing of
%          the water mask.
%
%
% Output
%
% profilOK: 0 means less than 20 pixel along the profil are outside the water
%           1 more than 20 pixel along the profil are outside the water
%
% profil: A binary mask of the same size as water_mask marking wetted pixel
% along the profil
%
% profilOK: 0 means less than 20 pixel along the profil are outside the water
%           1 more than 20 pixel along the profil are outside the water
% André Stumpf
% January 21, 2014

x_line = size(water_mask,2)/2;
nl = size(water_mask,1);
nc = size(water_mask,2);
point = [x_line  y_line];

% line equation
f = @(x) tan(angle)*(x-point(1)) + point(2);

% get idices along the line
linx = 1:nc;
liny = round(f(linx));
  
[~,yGrid] = meshgrid(1:nc,1:nl);
index = abs(yGrid-repmat(liny,size(yGrid,1),1)) <= 2*tol*(yGrid(2,1)-yGrid(1,1));
profil = water_mask==1 & index==1;

%check if there are any water pixel along the profil
if sum(profil(:))< 10*tol
    profilOK = 0;
    disp('No water detected along this profil')
    return
end

% intersect with aoi and check if there are sufficient pixel along the
% profil which are not covered by water
profil_aoi = aoi==1 & index==1;
outside_water = profil_aoi ~= profil;
profilOK = sum(outside_water(:))>20;
% profilOK = 1 % to deactivate check for the processing of Haliba videos

% %use only the largets region
% profil_props = regionprops(profil, 'Centroid','Area','PixelIdxList');
% [~,index] = max([profil_props.Area]);
% profil(:) =0;
% profil(profil_props(index).PixelIdxList)=1;

if doCheck == 1; doPlot = 1; end

if doPlot ==1
    figure
    subplot(2,2,1);
    img = mat2gray(img);
    imagesc(img)
    colormap('gray')
    hold on
    h = imagesc(water_mask); % show the edge image
    plot(linx, liny, 'Color', 'Red')	% plot line
    hold off
    set( h, 'AlphaData', .3 );
    title('Watermask');

    subplot(2,2,2);
    imagesc(img)
    colormap('gray')
    hold on
    h = imagesc(profil); % show the edge image
    plot(linx, liny, 'Color', 'Red')	% plot line
    hold off
    set( h, 'AlphaData', .3 );
    title('Selected pixel');
    
    bb = regionprops(profil,'BoundingBox');
    bb = struct2array(bb);
    xmin = min(bb(1:4:end))-10;
    xstep= bb(3:4:end);
    xmax = max(bb(1:4:end))+xstep(end)+10;
    ymin = min(bb(2:4:end))-10;
    ystep = max(bb(4:4:end));
    ymax = max(bb(2:4:end))+ystep+10;
    
%     if length(bb)>1
%         minbb = min(bb.BoundingBox);
%         maxbb = max(bb.BoundingBox);
%         xmin = minbb(1)-10;
%         xmax = maxbb(1)+maxbb(3)+10;
%         ymin = minbb(2)-10;
%         ymax = maxbb(2)+maxbb(4)+10;
%     else
%         bb=cat(1,bb.BoundingBox);
%         xmin = bb(1)-10;
%         xmax = bb(1)+bb(3)+10;
%         ymin = bb(2)-10;
%         ymax = bb(2)+bb(4)+10;
%     end
%     
    subplot(2,2,3);
    imagesc(img)
    axis([xmin, xmax, ymin, ymax])
    % axis equal
    colormap('gray')
    hold on
    h = imagesc(profil); % show the edge image
    plot(linx, liny, 'Color', 'Red')	% plot line
    hold off
    set( h, 'AlphaData', .3 );
    title('Selected pixel - Zoom');
end

if doCheck == 1;
    
    choice = menu('Is the water mask correct?','Yes','No - Open Editor to manually adjust the mask');

    switch choice
        case 1
             return
        case 2
            % roiwindow = CROIEditor(im(:,:,1));
            % addlistener(roiwindow, 'MaskDefined', @your_roi_defined_callback);
            
            figure(2)
            imagesc(img(:,:,1));
            colormap('gray')
            h = impoly;
            wait(h)
            mask = createMask(h);
           
            % handleEditor = findall(0, 'Name', 'Analyzer - ROI Editor');
            % waitfor(handleEditor)
            
            if exist('mask', 'var') && ~isempty(mask)
                index = abs(yGrid-repmat(liny,size(yGrid,1),1)) <= 2*tol*(yGrid(2,1)-yGrid(1,1));
                profil = mask==1 & index==1;
                figure(1)
                subplot(2,2,4);
                imagesc(img)
                axis([xmin, xmax, ymin, ymax])
                axis equal
                colormap('gray')
                hold on
                h = imagesc(profil); % show the edge image
                plot(linx, liny, 'Color', 'Red')	% plot line
                hold off
                set( h, 'AlphaData', .3 );
                title('Selected pixel - after editing');
            else
                disp('no new mask found!')
            end
        
        otherwise
             disp('Something went wrong!');
    end

end

end