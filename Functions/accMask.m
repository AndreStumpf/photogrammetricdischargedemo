function  [Pr, Rec,F] = accMask(mask, mask_gt, aoi)

%%accMask  Intersects two matrices and computes precision, recall and
%          and the F-measure.
%
%   [Pr, Rec,F] = accMask(mask, mask_gt)
%   Takes the mask (values > 0 ->  occurrence, 0 -> non-occureance) and
%   compares it with the ground truth of the mask to compute precission,
%   recall and the F-measure
%
%   [Pr, Rec,F] = accMask(mask, mask_gt, aoi)
%   Same as the previous but only the areas marked as 1 in the aoi matrix
%   are considered.
%
%
%   Input
%   
%   All input matrices must have the same extent. If the matrices have more
%   than two dimensions the first layer in the third dimension is
%   considered only. All values greater 0 are automatically converted to 1.
%
%
%   Output
%
%   Pr: Precission = true positives/(true positives+false positives)
%
%   Rec: Recall = true positives/(true positives+false negatives)
%
%   F: F-measure = F = 2*Pr*Rec/(Pr+Rec)
%
%	André Stumpf
%	Februar 02, 2015
                  
%check input variables
narginchk(2, 3)
nargoutchk(3, 3)

if size(mask,3)>1
    mask = mask(:,:,1);
end

if size(mask_gt,3)>1
    mask_gt = mask_gt(:,:,1);
end

% convert any input to double
mask_gt = double(mask_gt>0);
mask = double(mask>0);

if exist('aoi', 'var')
    if size(aoi,3)>1
       aoi = aoi(:,:,1);
    end
    mask_gt(~aoi) = NaN; %remove ground truth outside the area of interest
    mask(~aoi) = NaN; %remove mask outside the area of interest
end
            
    TP = nansum((mask_gt(mask==1))==1);
    FP = nansum((mask_gt(mask==1))==0);
    FN = nansum((mask_gt(mask==0))==1);
    
    if (TP+FP) == 0 %catch empty profiles leading to division by zero
        Pr=0;
    else
        Pr = TP/(TP+FP);
    end
    
    if (TP+FN) == 0 %catch empty profiles leading to division by zero
        Rec=0;
    else
        Rec = TP/(TP+FN);
    end 
    
    if (Pr+Rec) == 0 %catch empty profiles leading to division by zero
        F=0;
    else
        F = 2*Pr*Rec/(Pr+Rec);
    end

end