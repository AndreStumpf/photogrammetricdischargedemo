function [pos_base, pos_input]=tri_grille3(pos_base, pos_input, nc_input, nl_input)

bord=1;
r1=find((pos_input(:,2)>bord) & (pos_input(:,2)<(nl_input-bord)) & (pos_input(:,1)>bord) & (pos_input(:,1)<(nc_input-bord)));
pos_base=pos_base(r1,:);
pos_input=pos_input(r1,:);

