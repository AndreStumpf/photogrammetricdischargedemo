function [veloc, point_veloc] = surfaceVelocity(frames, crop, Cam_CalTsai,...
                                video, framdur, n_im, init_im, doPlot,...
                                window_size, step_size, corr_thresh)

%   surfaceVelocity  Takes a 3D matrix, coorelates the layers along the
%   third dimension, and computes the mean displacement at sub-pixel
%   precission in meter using the Tsai cameral model
%
%   [veloc, point_veloc] = surfaceVelocity(frames, crop)
%   Takes the 3D matrix frames, crops the first two dimensions as defined in
%   crop and correlates the remaining area of the layers along the third
%   dimension and returns a high resolution velocity field as well as
%   point-wise measurements
%   
%   [veloc, point_veloc] = surfaceVelocity(frames, crop, n_im, init_im, doPlot)
%   Same as before but instead of using all frames the routine starte at
%   the frame init_m and uses all all n_im subsequent frames. If doPlot is
%   set to 1 the resulting displacement field will be visualized.
%
%   ... further parameters see below
%  
%   Input
%   
%   frames: A 3D matrix holding greyscale images along the third dimension
%
%   crop: A vector indicating the number of pixels that should be cropped
%   from the frames on each side in the form [x_left, y_top, x_right,
%   y_bottom]
%
%   n_im: number of images from frames to be correlated, will generate a warning
%   message if choosen to high
%
%   init_im: image from framesto start with
%
%   doPlot: 0 by default > no plotting
%           1            > Two plots of the displacement field
%
%   window_size: The edge length of the correlation window. Default is 10.
%   [3...nl/2]
%
%   step_size: The distance in x and y between measuremed pixels. A higher
%   value can speed up the computation significantly. Default is 10.
%   [1...nl/2]
%
%   corr_thresh: An empirical threshold to ignore measurments with a
%   correlation value below it. Default is 0.6. [0...1]
%
%   threshCC: All points with a Correlation Coefficient [0-1] below this
%   value will be removed before interpolation.
%
%   Output:
%
%   veloc: A matrix with the dimensions of the input video holding along the
%   third dimension the displacement in X, Y and total
%
%   point_veloc: An nx4 matrix coordinates and displacement values of 
%   meausured pointsof in the form (x, y, delta_x, delta_y)
%
%	André Stumpf
%	January 26, 2015                             
                             
narginchk(2,11)
nargoutchk(2,2)

if ~exist('doPlot', 'var'), doPlot = 0; end

% set to all frames if number and starting images are not defined or set to
% 'all' and 'first' respectively
if ~exist('n_im', 'var'), n_im = 'all'; end
if strcmp(n_im, 'all');
    n_im = size(frames,3);
    init_im = 1;
end
if ~exist('init_im', 'var'), init_im = 'first'; end
if strcmp(init_im, 'first'); init_im = 1; end

% check if there are sufficient frames for the specified parameters
 iter_corr=init_im + n_im-2;
 if iter_corr > size(frames,3)-1
     iter_corr = size(frames,3)-init_im-1;
     disp('Warning! Defined frame sequence is longer than the length of the video!')
     disp('Only the available frames after init_im will be used. Press enter to continue.')
     pause
 end

% declare variables global
global CORRSIZE THRESHOLD pasxety

if ~exist('window_size', 'var')
    CORRSIZE=10;
else CORRSIZE=window_size;
end

if ~exist('step_size', 'var')
    pasxety=10;
else pasxety=step_size;
end

if ~exist('corr_thresh', 'var')
    THRESHOLD=0.6;
else THRESHOLD=corr_thresh;
end

if sum(size(crop) == [1, 4]) ~= 2, disp('Specified crop is not a 1x4 matrix!'), end;

% extract image for visualization
img = frames(:,:,1);

% crop images as specified
x_in = crop(1)+1;
y_in = crop(2)+1;
x_end = size(frames,2)-crop(3);
y_end = size(frames,1)-crop(4);

frames=frames(y_in:y_end,x_in:x_end,:);
[nl, nc]=size(frames(:,:,1));

% pre-allocation
nmax =  ceil(n_im * (nl/pasxety) * (nc/pasxety)); %estimate of the maximum number of points
res=zeros(nmax,5); 
npt_old=0;           

%correlation + adding the frame time steps for each point
disp('Running correlation...')
for i=init_im:iter_corr                                 % loop over all images, skip the last
    base=frames(:,:,i);                                 % select image time step i
    input=frames(:,:,i+1);                              % select image time step i+1
    [ipt_ok, bpt_ok]=Main_correl_2_im(base,input);      % ipt_ok et bpt_ok : are the positions of processed pixels in the input and the base image, respectively
    [npt, ~]=size(ipt_ok);                              % size better than lenght with the special case where size(ipt_ok)=1x2   
    res(npt_old+1:npt_old+npt,:)=[bpt_ok ipt_ok repmat(framdur(i), npt,1)];       % concatenation into one single table
    npt_old=npt+npt_old;
end

% reshape the output
res(res(:,1)==0,:)=[];    % delete the unused rows at the end of the matrix that remain 0
                          % now comes the tricky part
res_sort=sortrows(res);   % sort the table by the xy coordinates in increasing order, in this way the same pixels in different images are grouped together
a=diff(res_sort(:,2));    % search through the y column for changes of the y coordinate
I=find(a~=0);             % a change in the y coordinate marks a new point
npt=length(I);            % count the number of pixels for which the displacement has been computed

% compute the displacement as the mean over all images > pixel/s !!!
ref_old=1;                  % initialize index that marks where the point coordinates start
res_fin=zeros(nmax,4);

for i=1 : npt
    ref=I(i);                                       % last occurence of the i-th point
    data=res_sort(ref_old:ref,:);                   % get all coordinates for this point
    U=mean((data(:,3)-data(:,1))./data(:,5));       % mean displacement in pixel/s in x
    V=mean((data(:,4)-data(:,2))./data(:,5));       % mean displacement in pixel/s in y
    res_fin(i,:)=[data(1,1:2) U V];                 % store point coordinates together with mean displacement
    ref_old=ref+1;                                  % update starting index for the next point
end

res_fin(res_fin(:,1)==0,:)=[];        % delete all lines at the end of res_fin which are still zero

% output at points
point_veloc = res_fin;

%% rough estimate of the ground velocities

VpGround=res_fin; % make a copy of the same size to store outputs

% account for the cropped area
VpGround(:,1) = VpGround(:,1) + crop(1);
VpGround(:,2) = VpGround(:,2) + crop(2);

% matrix for reprojection corresponding to original image size
[X_im, Y_im]=meshgrid(1:1:Cam_CalTsai{1},1:1:Cam_CalTsai{2});

% compute ground coordinates of the the pixel according to mean GPS
% coordinates
z_water = -Cam_CalTsai{14};
Pts2D=[X_im(:) Y_im(:)];
PtsZ = ones(length(Pts2D),1)*z_water ; % Z = 0;
Pts3DProj = Image2GroundTsai(Pts2D, PtsZ, Cam_CalTsai);

% compute resolution at the ground
X_sol_mat=reshape(Pts3DProj(:,1), Cam_CalTsai{2}, Cam_CalTsai{1});
Y_sol_mat=reshape(Pts3DProj(:,2), Cam_CalTsai{2}, Cam_CalTsai{1});
resol_x=diff(X_sol_mat);
resol_y=diff(Y_sol_mat);

% compute the ground velocities as image velocity * ground resolution
for i=1:1:size(VpGround(:,1)) %for all points
    y_im=VpGround(i,1);       % image coordinates
    x_im=VpGround(i,2);
    VpGround(i,3)=-resol_x(x_im,y_im)*VpGround(i,3); % ground resolution(for image x image y) * displacement / time step
    VpGround(i,4)=resol_y(x_im,y_im)*VpGround(i,4); 
end

% velocities
U_gr=VpGround(:,3);
V_gr=VpGround(:,4);

% Interpolation of the measured vectors (ground/m) to the resolution of the image
Q_U_gr = griddata(VpGround(:,1), VpGround(:,2), U_gr, X_im, Y_im, 'natural');
Q_V_gr = griddata(VpGround(:,1), VpGround(:,2), V_gr, X_im, Y_im, 'natural');
Q_tot_gr = griddata(VpGround(:,1), VpGround(:,2), (V_gr.^2+U_gr.^2).^(0.5), X_im, Y_im, 'natural');

veloc = cat(3, Q_U_gr, Q_V_gr, Q_tot_gr);
%% plot
if doPlot==1
   clf
   subplot(1,2,1)
   % imagesc(img);  hold on; quiver(res_fin(:,1)+crop(1),res_fin(:,2)+crop(2), res_fin(:,3),res_fin(:,4),'blue'); hold off
   imagesc(img);  hold on; quiver(VpGround(:,1),VpGround(:,2),VpGround(:,3),VpGround(:,4),'blue'); hold off
   axis equal; axis tight;
   colormap('gray')
   title('Measured displacement in image geometry', 'FontSize', 12)
   freezeColors
   
   subplot(1,2,2)      
   % imagesc(Q_tot);  hold on; quiver(res_fin(:,1),res_fin(:,2), res_fin(:,3),res_fin(:,4),'black'); hold off
   imagesc(Q_tot_gr);  hold on; quiver(VpGround(:,1),VpGround(:,2),VpGround(:,3),VpGround(:,4),'black'); hold off
   axis equal; axis tight;
   % title('Total displacement in pixel', 'FontSize', 12)
   title('Total displacement in m/s', 'FontSize', 12)
   colormap('hot')
   colorbar
   freezeColors
end

end