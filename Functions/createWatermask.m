function water_mask = createWatermask(video, frames, veloc, flowtype,...
                                    firstFrame, nFrames, crop, doPlot)


disp('Generating water mask...')
% pre-allocate
greenDiff=zeros(video.Height,video.Width,nFrames, 'uint8'); 
Brightness=zeros(video.Height,video.Width,nFrames, 'uint8');

% set variables
nFrames = size(frames,3);
pas=1;
n=0;

for k = firstFrame:pas:(firstFrame+nFrames)
    n=n+1;
    mov(k).cdata = read(video, k);                                         % read the images into a structure
    im_int=double(mov(k).cdata);                                           % reform the images into a matrix
    greenDiff(:,:,n) = im_int(:,:,2) - (im_int(:,:,1) + im_int(:,:,3))/2;  % greenDifference = greenChannel - (redChannel + blueChannel)/2;
    Brightness(:,:,n) = mean(im_int,3);
end

% compute mean along the third dimension
greenDiff = mean(greenDiff,3);
Brightness = mean(Brightness,3);

% Compute standard deviation over time
im_std = double(frames);
im_std = std(im_std,[],3);

% crop as specified
x_in = crop(1)+1;
y_in = crop(2)+1;
x_end = size(frames,2)-crop(3);
y_end = size(frames,1)-crop(4);

frames=frames(y_in:y_end,x_in:x_end,:);
im_std = im_std(y_in:y_end,x_in:x_end,:);
Brightness = Brightness(y_in:y_end,x_in:x_end,:);
greenDiff = greenDiff(y_in:y_end,x_in:x_end,:);
v_tot = veloc(y_in:y_end,x_in:x_end,3);

% set pixel that show surface velocities above 6 m/s to 0
% this is mainly to adress object moving in the close foreground of the
% camera, e.g. insects
v_tot(v_tot > 6) = 0; 

% Compute and threshold local entropy
NHOOD = strel('disk', 5, 0);
NHOOD  = getnhood(NHOOD);
im_local_ent = entropyfilt(frames(:,:,1:3:nFrames), NHOOD); %take only every third image to speed up computation
im_local_ent = mean(im_local_ent,3);
% im_local_ent_bin = im_local_ent  <= 4;

% normalize the indicators
Brightness_norm = (Brightness-min(Brightness(:))) ./ (max(Brightness(:))-min(Brightness(:)));
im_std_norm = (im_std-min(im_std(:))) ./ (max(im_std(:))-min(im_std(:)));
v_tot_norm = (v_tot-min(v_tot(:))) ./ (max(v_tot(:))-min(v_tot(:)));
im_local_ent_norm = (im_local_ent-min(im_local_ent(:))) ./ (max(im_local_ent(:))-min(im_local_ent(:)));
%im_local_ent_norm(Brightness<50) = 1; % set entropy to zero where brightness <50
greenDiff_norm = (greenDiff-min(greenDiff(:))) ./ (max(greenDiff(:))-min(greenDiff(:)));

% comine the indicators according to the selected flowtype
switch flowtype
    case 1 % low flow
        %WaterIndex = im_std_norm + v_tot_norm + (im_local_ent_bin .* im_local_ent_norm);
        
        if sum(sum(isnan(greenDiff_norm))) == numel(greenDiff) % catch the rare case of black&white input video
           WaterIndex = (im_std_norm + v_tot_norm + (1- im_local_ent_norm))/4;
        else
           WaterIndex = (im_std_norm + v_tot_norm + (1- im_local_ent_norm) - greenDiff_norm)/4;
        end
        
    case 2 % high flow
        WaterIndex = (im_std_norm + v_tot_norm + Brightness_norm)/3; 
        %WaterIndex = (im_std_norm + v_tot_norm - greenDiff_norm)/3;
    otherwise
        disp('Value for flowtype is not valid')     
end
    
% automatic thresholding to initialize mask
level = graythresh(WaterIndex(WaterIndex < 0.3));
WaterMask_init = WaterIndex >= level;
WaterMask_init(Brightness<50) = 0;
% clean up watermask with activecontours and morphological filtering
WaterIndex(isnan(WaterIndex)) = 0;
WaterMask_final = activecontour(WaterIndex, WaterMask_init, 'Chan-Vese', 1);
WaterMask_final = bwareaopen(WaterMask_final, 2000, 4);
WaterMask_final = ~(bwareaopen(~WaterMask_final, 600, 4));

%% test if detected water is significantly brighter than the surrounding
BrightnessIn = Brightness_norm(WaterMask_final);
% hist(BrightnessIn)
BrightnessOut = Brightness_norm(~WaterMask_final);
% hist(BrightnessOut)
h = ttest2(BrightnessIn,BrightnessOut, 'Vartype','unequal');

if h==0 && mean(BrightnessIn) > mean(BrightnessOut) % if the brightness of the water is signfificantly higher
    WaterIndex = (im_std_norm + v_tot_norm + (1- im_local_ent_norm) - greenDiff_norm + Brightness_norm)/5;    
    % automatic thresholding to initialize mask
    level = graythresh(WaterIndex(WaterIndex < 0.3));
    WaterMask_init = WaterIndex >= level;
    % clean up watermask with activecontours and morphological filtering
    WaterIndex(isnan(WaterIndex)) = 0;
    WaterMask_final = activecontour(WaterIndex, WaterMask_init, 'Chan-Vese', 1);
    WaterMask_final = bwareaopen(WaterMask_final, 2000, 4);
    WaterMask_final = ~(bwareaopen(~WaterMask_final, 600, 4));
end


water_mask = padarray(WaterMask_final,[crop(2) crop(1)], 'pre');
water_mask = padarray(water_mask,[crop(4) crop(3)], 'post');

%% PLOT
if doPlot==1
    figure
    img = mat2gray(im_int(:,:,1));
    imagesc(img)
    colormap('gray')
    hold on
    h = imagesc(water_mask); % show the edge image
    hold off
    set( h, 'AlphaData', .3 );
    if flowtype == 1; title('Water mask - Low flow'); end
    if flowtype == 2; title('Water mask - High flow'); end
end


end