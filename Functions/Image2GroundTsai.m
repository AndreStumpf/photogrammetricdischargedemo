function Pts3DProj = Image2GroundTsai(Pts2D, PtsZ, Cam_CalTsai)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%upix=Cam_CalTsai{1};
%vpix=Cam_CalTsai{2};
dx=Cam_CalTsai{3}*1000; % from m to mm
dy=Cam_CalTsai{4}*1000; % from m to mm
R=Cam_CalTsai{5};
T=Cam_CalTsai{6};
f=Cam_CalTsai{7}*1000; % from m to mm
sx=Cam_CalTsai{8};
u0=Cam_CalTsai{9};
v0=Cam_CalTsai{10};
k1=Cam_CalTsai{11};
% get offset to real world coordinates
deltax=Cam_CalTsai{12};
deltay=Cam_CalTsai{13};
deltaz=Cam_CalTsai{14};

% distance among the pixel centers in mm
dpx=dx; % *Ncx/Nfx;
dpy=dy;

% Camera center = transpose(R) * -T, CC=(Cx, Cy, Cz)'
CC=(R'*-T);
CC=CC(1:3);

% make calibration matrix, only focal since pp offset already accounted for
K= [f 0 0; 0 f 0; 0 0 1];
% rotation matrix to 3x3
Rot = R(1:3,1:3);

Pts2D=Pts2D'; % transpose input
PtsZ=PtsZ+deltaz; %translation to zero origin
PtsZ=PtsZ'*1000; %conversion to mm

% number of points 
n=size(Pts2D,2);

% Step1: Pixel to metric coordinates
Xd_b=-dpx*(Pts2D(1,:)-u0)/sx;
Yd_b=-dpy*(Pts2D(2,:)-v0);

% Step2: Correct lens distorsion
Xu_b=Xd_b+k1.*(Xd_b.^2+Yd_b.^2).*Xd_b;
Yu_b=Yd_b+k1.*(Xd_b.^2+Yd_b.^2).*Yd_b;

% Step3: compute scale factors, Morvan, PhD thesis, 2009, Eq. 2.20
% scaling factor lambda = (Z-CCz)/z3
% (z1,z2,z3)' = R^-1*K^-1*p
% p is an image point in homologous coordinates
p = [Xu_b; Yu_b; ones(1,n)];
% p = [Xu_b(1); Yu_b(1); 1];
pz = Rot^-1*K^-1*p;
lambda = (PtsZ-CC(3))./pz(3,:);

% Step4: project ray on the plane according to the scale factor
% ray equation (X,Y,Z)' = C+lambda*R^-1*R^-1*p
% Morvan, PhD thesis, 2009, Eq. 2.19
Pts3DProj = zeros(3,n);
for i=1:1:n
   Pts3DProj(:,i)=CC+lambda(i)*Rot^-1*K^-1*p(:,i);
end

% and back to meters
Pts3DProj=Pts3DProj'/1000;

% and back to real world coordinates
dxyz = repmat([deltax; deltay; deltaz]',size(Pts2D,2),1);
Pts3DProj = Pts3DProj - dxyz;
end

% %DEVELOPEMENT CODE
% % Step1: Pixel to metric coordinates
% Xd_r=-dpx*(Pts2D(1,:)-u0)/sx;
% Yd_r=-dpy*(Pts2D(2,:)-v0);
% 
% % Step2: Correct lens distorsion
% Xu_r=Xd_r+k1.*(Xd_r.^2+Yd_r.^2).*Xd_r;
% Yu_r=Yd_r+k1.*(Xd_r.^2+Yd_r.^2).*Yd_r;
% Xu_r = Xu_r';  % reshape into a column vector
% Yu_r = Yu_r';  % reshape into a column vector
% 
% % Camera center = transpose(R) * -T, CC=(Cx, Cy, Cz)'
% CC=(R'*-T);
% % make calibration matrix, only focal since pp offset already accounted for
% K= [f 0 0; 0 f 0; 0 0 1];
% 
% %(z1,z2,z3)' = R^-1*K^-1*p
% % p is an image point in homologous coordinates
% p = [Xu_r(1),Yu_r(1),1]';
% pz = R(1:3,1:3)^-1*K^-1*p;
% 
% % scaling factor lambda = (Z-CCz)/z3
% lambda = (Pts3D(3,1)-CC(3))/pz(3);
% 
% % ray equation (X,Y,Z)' = C+lambda*R^-1*R^-1*p
% p3D = CC(1:3)+lambda*R(1:3,1:3)^-1*K^-1*p;